package cl.ufro.dci.compraVenta.model;

public class Person {
    private String rut;
    private String name;

    public Person() {
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "rut='" + rut + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
