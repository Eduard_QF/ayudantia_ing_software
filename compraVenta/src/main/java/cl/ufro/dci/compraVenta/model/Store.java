package cl.ufro.dci.compraVenta.model;

import java.util.ArrayList;

public class Store {
    private static Store store;
    private ArrayList<Car> cars;
    private ArrayList<Person> people;

    public static Store getStore() {
        if (store == null){
            store = new Store();
        }
        return store;
    }

    private Store(){
        cars = new ArrayList<>();
        people = new ArrayList<>();
    }

    public ArrayList<Car> getCars() {
        return cars;
    }

    public void addVehiculos(Car car) {
        this.cars.add(car);
    }

    public ArrayList<Person> getPeople() {
        return people;
    }

    public void addPerson(Person person) {
        this.people.add(person);
    }
}
