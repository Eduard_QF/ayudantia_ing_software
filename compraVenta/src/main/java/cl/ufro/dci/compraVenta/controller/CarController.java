package cl.ufro.dci.compraVenta.controller;

import cl.ufro.dci.compraVenta.model.Car;
import cl.ufro.dci.compraVenta.model.Store;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;


@Controller
@RequestMapping(value = "/cars")
public class CarController {
    Store store = Store.getStore();

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String getIndex(Model model){
        model.addAttribute("cars",store.getCars());
        model.addAttribute("ows",store.getPeople().toArray());
        model.addAttribute("bs",store.getPeople().toArray());
        return "/car";
    }

    @RequestMapping(value = "/",method = RequestMethod.POST)
    public String postIndex(@ModelAttribute Car car){
        store.addVehiculos(car);
        return "redirect:/cars/";
    }

    /**
     * Funcion para eliminar un vehiculo.
     * Obtenemos la patente por la url, y la buscamos para eliminarla.
     */
    @RequestMapping(value = "/delete/{enrollment}",method = RequestMethod.GET)
    public String getDelete(@PathVariable String enrollment){
        ArrayList<Car> cars=store.getCars();
        for (Car car: cars){
            if (car.getEnrollment().equals(enrollment)){
                store.getCars().remove(car);
                break;
            }
        }
        return "redirect:/cars/";
    }

    @RequestMapping(value = "/edit/{enrollment}",method = RequestMethod.GET)
    public String getEdit(Model model,@PathVariable String enrollment){
        Car tmp= new Car();
        for (Car car: store.getCars()){
            if (car.getEnrollment().equals(enrollment)){
                tmp =  car;
                break;
            }
        }
        model.addAttribute("car",tmp);
        return "editCars";
    }

    @RequestMapping(value = "/edit/{enrollment}",method = RequestMethod.POST)
    public String postEdit(@PathVariable String enrollment, @ModelAttribute Car newCar){

        for (Car car: store.getCars()){
            if (car.getEnrollment().equals(enrollment)){
                store.getCars().remove(car);
                store.addVehiculos(newCar);
                break;
            }
        }
        return "redirect:/cars/";
    }

}
