package cl.ufro.dci.compraVenta.model;

public class Car {
    private String enrollment;
    private String model;
    private Person owner;
    private Person buyer;

    public Car() {
    }

    public String getEnrollment() {
        return enrollment;
    }

    public void setEnrollment(String enrollment) {
        this.enrollment = enrollment;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public Person getBuyer() {
        return buyer;
    }

    public void setBuyer(Person buyer) {
        this.buyer = buyer;
    }

    @Override
    public String toString() {
        return "Car{" +
                "enrollment='" + enrollment + '\'' +
                ", model='" + model + '\'' +
                ", owner=" + owner +
                ", buyer=" + buyer +
                '}';
    }
}
