package cl.ufro.dci.compraVenta.controller;

import cl.ufro.dci.compraVenta.model.Car;
import cl.ufro.dci.compraVenta.model.Person;
import cl.ufro.dci.compraVenta.model.Store;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;

@Controller
@RequestMapping(value = "/people")
public class PeopleController {

    Store store = Store.getStore();

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String getIndex(Model model){
        model.addAttribute("people",store.getPeople());
        return "/person";
    }

    @RequestMapping(value = "/",method = RequestMethod.POST)
    public String postIndex(@ModelAttribute Person person){
        store.addPerson(person);
        return "redirect:/people/";
    }

    @RequestMapping(value = "/delete/{rut}",method = RequestMethod.GET)
    public String getDelete(@PathVariable String rut){
        ArrayList<Person> pers=store.getPeople();
        for (Person per: pers){
            if (per.getRut().equals(rut)){
                store.getCars().remove(per);
                break;
            }
        }
        return "redirect:/people/";
    }

    @RequestMapping(value = "/edit/{rut}",method = RequestMethod.GET)
    public String getEdit(@PathVariable String rut, Model model){
        Person pers = new Person();
        for (Person per: store.getPeople()){
            if (per.getRut().equals(rut)){
                pers = per;
                break;
            }
        }
        model.addAttribute("person",pers);
        return "editPeople";
    }

    @RequestMapping(value = "/edit/{rut}",method = RequestMethod.POST)
    public String postEdit(@PathVariable String rut,@ModelAttribute Person newPer){

        for (Person per: store.getPeople()){
            if (per.getRut().equals(rut)){
                store.getPeople().remove(per);
                store.addPerson(newPer);
                break;
            }
        }
        return "redirect:/people/";
    }

}
