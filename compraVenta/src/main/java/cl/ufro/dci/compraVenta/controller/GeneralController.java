package cl.ufro.dci.compraVenta.controller;

import cl.ufro.dci.compraVenta.model.Car;
import cl.ufro.dci.compraVenta.model.Person;
import cl.ufro.dci.compraVenta.model.Store;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@Controller
public class GeneralController {
    Store store = Store.getStore();
    ArrayList<Car> sales= new ArrayList<>();

    @RequestMapping(value="/",method = RequestMethod.GET)
    public String getIndex(Model model){
        model.addAttribute("cars",store.getCars());
        model.addAttribute("people",store.getPeople());
        model.addAttribute("sales",sales);
        return "index";
    }


    @RequestMapping(value="/",method = RequestMethod.POST)
    public String postIndex(@RequestParam String car, @RequestParam String owner,@RequestParam String buyer){
        Person tmpOwner= new Person();
        Person tmpBuyer= new Person();
        for (Person per: store.getPeople()){
            if (per.getRut().equals(owner)){
                tmpOwner = per;
            }
            if (per.getRut().equals(buyer)){
                tmpBuyer = per;
            }
        }
        for (Car tmpcar: store.getCars()){
            if (tmpcar.getEnrollment().equals(car)){
                tmpcar.setOwner(tmpOwner);
                tmpcar.setBuyer(tmpBuyer);
                sales.add(tmpcar);
            }
        }
        return "redirect:/";
    }
}
