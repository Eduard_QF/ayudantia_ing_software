package cl.ufro.dci.Veterinaria.modelo;

import java.util.ArrayList;

public class Veterinaria {
    private static Veterinaria veterinaria;

    public static Veterinaria getInstance(){
        if (veterinaria == null){
            veterinaria = new Veterinaria();
        }
        return veterinaria;
    }

    private ArrayList<Usuario> usuarios;

    private Veterinaria() {
        this.usuarios = new ArrayList<>();
    }

    public ArrayList<Usuario> getUsuarios() {
        return usuarios;
    }

    public void addUsuario(Usuario usuario) {
        this.usuarios.add(usuario);
    }
}
