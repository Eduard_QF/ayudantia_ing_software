package cl.ufro.dci.Veterinaria.controller;

import cl.ufro.dci.Veterinaria.modelo.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/mascotas")
public class MascotaController {
    private Veterinaria veterinaria = Veterinaria.getInstance();
    private Usuario duenio;

    @RequestMapping(value="/{usuario}",method = RequestMethod.GET)
    public String getIndex(Model model,@PathVariable("usuario") String rut){
        System.out.println("usuario :"+rut);
        for (Usuario usr : veterinaria.getUsuarios()){
            if (usr.getRut().equals(rut)){
                duenio=usr;
            }
        }
        model.addAttribute("tipos", TipoMascota.values());
        model.addAttribute("mascotas",duenio.getMascotas());
        return "mascota/Mascota";
    }

    @RequestMapping(value = "/{rut}",method = RequestMethod.POST)
    public String postIndex(Model model, @ModelAttribute Mascota mascota, @PathVariable String rut){
        System.out.println("mascota: "+mascota);
        duenio.addMascotas(mascota);
        return "redirect:/mascotas/"+rut;
    }
}
