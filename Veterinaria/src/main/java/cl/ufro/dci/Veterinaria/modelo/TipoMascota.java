package cl.ufro.dci.Veterinaria.modelo;

public enum TipoMascota {
    PERRO("Perro"),
    GATO("Gato"),
    LORO("Loro"),
    HAMSTER("Hamster"),
    CONEJO("Conejo"),
    CULLI("Culli"),
    ERIZO("Erizo");

    private final String tipo;
    TipoMascota(String tipo){
        this.tipo=tipo;
    }

    public String getTipo() {
        return tipo;
    }
}
