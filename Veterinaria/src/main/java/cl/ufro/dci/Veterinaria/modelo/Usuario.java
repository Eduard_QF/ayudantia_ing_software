package cl.ufro.dci.Veterinaria.modelo;

import java.util.ArrayList;

public class Usuario {
    private String nombre;
    private String rut;
    private String correo;
    private int edad;
    private ArrayList<Mascota> mascotas;

    public Usuario() {
        mascotas = new ArrayList<>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public ArrayList<Mascota> getMascotas() {
        return mascotas;
    }

    public void addMascotas(Mascota mascotas) {
        this.mascotas.add(mascotas);
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "nombre='" + nombre + '\'' +
                ", rut='" + rut + '\'' +
                ", correo='" + correo + '\'' +
                ", edad=" + edad +
                '}';
    }
}
