package cl.ufro.dci.Veterinaria.controller;

import cl.ufro.dci.Veterinaria.modelo.Usuario;
import cl.ufro.dci.Veterinaria.modelo.Veterinaria;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;

@Controller
@RequestMapping(value = "/usuarios")
public class UsuarioController {
    private Veterinaria veterinaria= Veterinaria.getInstance();

    @RequestMapping(value="/",method = RequestMethod.GET)
    public String getIndex(Model model){
        model.addAttribute("usuarios",veterinaria.getUsuarios());
        return "/usuario/Usuario";
    }

    @RequestMapping(value = "/",method = RequestMethod.POST)
    public String postIndex(Model model, @ModelAttribute Usuario user){
        veterinaria.addUsuario(user);
        return "redirect:/usuarios/";
    }
}
