package cl.ufro.dci.Veterinaria.controller;

import cl.ufro.dci.Veterinaria.modelo.Usuario;
import cl.ufro.dci.Veterinaria.modelo.Veterinaria;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;

@RestController
@RequestMapping(value="/usuarios")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class UsuarioController {
    private Veterinaria veterinaria= Veterinaria.getInstance();

    @RequestMapping(value="/",method = RequestMethod.GET, produces = "application/json")
    public ArrayList<Usuario> getIndex(){
        return veterinaria.getUsuarios();
    }

    @RequestMapping(value = "/",method = RequestMethod.POST, produces = "application/json")
    public String postIndex( @RequestBody Usuario user){
        veterinaria.addUsuario(user);
        return "status:200";
    }
}
