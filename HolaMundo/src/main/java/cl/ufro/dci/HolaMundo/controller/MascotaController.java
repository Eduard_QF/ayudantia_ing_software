package cl.ufro.dci.HolaMundo.controller;

import cl.ufro.dci.HolaMundo.modelo.Mascota;
import cl.ufro.dci.HolaMundo.modelo.Usuario;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MascotaController {
    @RequestMapping(value="/mascotas",method = RequestMethod.GET)
    public String getIndex(){
        return "/Mascota";
    }

    @RequestMapping(value = "/mascotas",method = RequestMethod.POST)
    public String postIndex(Model model, @ModelAttribute Mascota mascota){
        System.out.println("mascota: "+mascota);
        model.addAttribute("mascota",mascota);
        return "/ResultadosMascota";
    }
}
