package cl.ufro.dci.HolaMundo.controller;

import cl.ufro.dci.HolaMundo.modelo.Usuario;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UsuarioController {

    @RequestMapping(value="/",method = RequestMethod.GET)
    public String getIndex(){
        return "/Index";
    }

    @RequestMapping(value = "/",method = RequestMethod.POST)
    public String postIndex(Model model, @ModelAttribute Usuario user){
        System.out.println("user"+user);
        model.addAttribute("usuario",user);
        return "/Resultados";
    }
}
